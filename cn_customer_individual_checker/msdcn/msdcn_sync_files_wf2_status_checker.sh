#echo "" > status.txt
echo "*MSD CN Daily Sync Files WF-2 Checker for Today:* \n" > status.txt

#Define customer Rundeck URL, Token and dataloading JOB ID
declare -A msdcn_cn_map=(["RD_URL"]="https://ops-cn.aktana.com" ["RD_TOKEN"]="1DeYTXWIFcz8x8wAwqJRmvblnQMugVz5" ["RD_JOB_ID"]="1f9cc116-f616-4417-98b8-9c9093cb31d3" ["NAME"]="MSD CN Sync_Load_Data_Files_Daily_Workflow-2" ["PROJECT"]="MSD")

#Define PagerDuty Service Key
SERVICE_KEY="ade256257b6d4a87b5d053529452999c"

customer=msdcn_cn_map

#Get latest job id
eval token=\${${customer}[\"RD_TOKEN\"]}
eval url=\${${customer}[\"RD_URL\"]}
eval job_id=\${${customer}[\"RD_JOB_ID\"]}
eval project=\${${customer}[\"PROJECT\"]}
id=`curl \
  -X "GET" -H "Accept: application/json" \
  -H "Content-Type: application/json" \
  -H "X-Rundeck-Auth-Token: ${token}" \
  "${url}/api/15/job/${job_id}/executions" |  jq .executions[0].id`

running_id=`curl -X GET -H "Accept: application/json" \
  -H "Content-Type: application/json" \
  -H "X-Rundeck-Auth-Token: ${token}" \
  "${url}/api/15/project/${project}/executions/running" | jq ".executions | .[] | select(.job.id == \"$job_id\")" | jq '.id'`

if [ -z "$running_id" ]
then
      echo "No Running execution"
else
      id=$running_id
fi

date=`curl \
  -X "GET" -H "Accept: application/json" \
  -H "Content-Type: application/json" \
  -H "X-Rundeck-Auth-Token: ${token}" \
  "${url}/api/15/job/${job_id}/executions" |  jq .executions[0] | jq '.["date-started"]'.date`

#Format date
date=${date:1:10}

today=`date -u "+%Y-%m-%d"`

#Get execution detail, identify steps failure
step_status=`curl \
            -X "GET" -H "Accept: application/json" \
            -H "Content-Type: application/json" \
            -H "X-Rundeck-Auth-Token: ${token}" \
 "${url}/api/15/execution/${id}/state" | jq .nodes[] | jq .[].executionState`

#Set job status
if [[ $step_status == *"FAILED"* ]]; then
  job_status="Failed"
else
  job_status="OK"
fi

if [[ ! -z "$running_id" ]]; then
  job_status="Running"
fi

#Print status
eval name=\${${customer}[\"NAME\"]}
eval project=\${${customer}[\"PROJECT\"]}

if [[ "$date" != "$today" ]] && [[ "$job_status" != "Running" ]]; then
echo "${name}: *\`Not Started\`* \n" >> status.txt

# Trigger PagerDuty
# curl -H "Content-type: application/json" -X POST     -d "{ `
# `            \"routing_key\": \"$SERVICE_KEY\",`
# `            \"event_action\": \"trigger\",`
# `            \"images\": [],`
# `            \"links\": [],`
# `            \"payload\": {`
# `                \"summary\": \"$name dataloading not started.\",`
# `                \"source\": \"dataloading checker\",`
# `                \"custom_details\": \"$name dataloading not started\",`
# `                \"severity\": \"critical\"`
# `            }`
# `    }"     "https://events.pagerduty.com/v2/enqueue"

elif [[ "$job_status" == "Failed" ]]; then
echo "${name}: *\`${job_status}\`* - ${url}/project/${project}/execution/show/${id} \n" >> status.txt

output=`curl \
            -X "GET" -H "Accept: application/json" \
            -H "Content-Type: application/json" \
            -H "X-Rundeck-Auth-Token: ${token}" \
 "${url}/api/15/execution/${id}/output"`

 # Adding failure reason check
 if [[ $output == *"Incorrect datetime value"* ]]; then
   echo "> Error detected: \`Incorrect datetime value\` \n" >> status.txt
 fi

 if [[ $output == *"NonZeroResultCode"* ]]; then
   echo "> Error detected: \`NonZeroResultCode\` \n" >> status.txt
 fi

 if [[ $output == *"UserWarning: Data Validation extension is not supported and will be removed warn(msg)"* ]]; then
   echo "> Error detected: \`UserWarning: Data Validation extension is not supported and will be removed warn(msg)\` \n" >> status.txt
 fi

elif [[ "$job_status" == "Killed" ]]; then
echo "${name}: *\`${job_status}\`* - ${url}/project/${project}/execution/show/${id} \n"
else
echo "${name}: *${job_status}* - ${url}/project/${project}/execution/show/${id} \n" >> status.txt
fi

echo dataloading=`cat status.txt` > ${WORKSPACE}/var.properties
